lines(0);
old_funcprot = funcprot();
funcprot(0);

demodir = get_absolute_file_path('derivativecheck.dem.sce');
exec(demodir+'broydentridiagonal.sci');
n = 1000; 
x0 = -ones(n,1);
[f,J] = broydentridiagonal(x0);
[coloring,numcolors,mincolors] = columncolor(J);
Jestimate = sfdjac(broydentridiagonal,x0,spones(J),coloring);
checkderivative(Jestimate,J)

funcprot(old_funcprot);

