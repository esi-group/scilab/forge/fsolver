// the pipe test problem on the scilav fsolve function

// Load the necessary functions (plot_pipe_graph, etc ...)
getd('.');

Plot = %T;
UseOptim = %T;

params = init_param();

params = add_param(params, 'Qmax', 250); // m^3/s
params = add_param(params, 'Pmax', 30);  // Bars
params = add_param(params, 'Pmin', 0);   // Bars
params = add_param(params, 'k_pipe', 5/100); 
params = add_param(params, 'Debug', %F); 

// Objective function for fsolve
function res = fsolve_obj_pipe(x)
  res = compute_pipe_eq(x,g_in,I_sources,params);
endfunction

// Analytical gradient for fsolve
function res = fsolve_dobj_pipe(x)
  res = full(compute_pipe_deq(x,g_in,I_sources,params)');
endfunction

// Function and Jacobian for fsolver
function [f,J] = fsolver_pipe(x)
       f = compute_pipe_eq(x,g_in,I_sources,params);
       [nargout,nargin] = argn();
       if nargout > 1 
	 J = compute_pipe_deq(x,g_in,I_sources,params)';
       end
endfunction


// Finite differences gradient for fsolve
function res = fsolve_dobj_diff_pipe(x)
  res = derivative(fsolve_obj_pipe,x);
endfunction

// Objective function for optim
function [y, dy, inp] = optim_pipe(x,inp)
  y_tmp = fsolve_obj_pipe(x);
  y = sum(y_tmp.^2);
  dy = fsolve_dobj_pipe(x);
  //dy = fsolve_dobj_diff_pipe(x);
  dy = 2 * y_tmp'*dy;
endfunction

//////////////////////////////////////
// Initialization of the pipe graph //
//////////////////////////////////////

// Available tests:
// - 'simple'
// - 'simple_loop_1'
// - 'simple_loop_2'
// - 'simple_loop_3'
// - 'test_hc_1'
// - 'test_hc_1_bis'
// - 'test_hc_2'
// - 'test_hc_3'
// - 'test_pr1'
// - 'test_pr2'
// - 'test_pr3'

[g_in, I_sources, I_leaves] = build_test('simple_loop_1',%F);

///////////////////////////////////////////////////
// Initialization of the flow and pressures (x0) //
///////////////////////////////////////////////////

[x0, lower, upper] = pipe_init(g_in, I_sources,params);

////////////////
// Resolution //
////////////////

// Without Jacobian and with fsolve
tic();
[x_opt, res_opt, info] = fsolve(x0,fsolve_obj_pipe,1e-6);
t = toc();

g_opt = pipe_update_graph(g_in,x_opt);
if Plot then subplot(2,2,1); plot_pipe_graph(g_opt,'test for fsolve - without Jacobian','X','Y'); end

printf('without Jacobian - result: info = %d, norm(res) = %f, elapsed time = %f\n',info, norm(res_opt), t);

// With analytical Jacobian and with fsolve
tic();
[x_opt, res_opt, info] = fsolve(x0,fsolve_obj_pipe,fsolve_dobj_pipe,1e-6);
t = toc();

g_opt = pipe_update_graph(g_in,x_opt);
if Plot then subplot(2,2,2); plot_pipe_graph(g_opt,'test for fsolve - with analytical Jacobian','X','Y'); end

printf('with Jacobian - result: info = %d, norm(res) = %f, elapsed time = %f\n',info, norm(res_opt), t);

// With analytical Jacobian and with fsolver
options = init_param();
options = add_param(options,'Tol',1e-6);
options = add_param(options,'Jacobian','on');
//options = add_param(options,'DerivativeCheck','on');

tic();
[x_opt, res_opt, info] = fsolver(fsolver_pipe,x0,options);
t = toc();

g_opt = pipe_update_graph(g_in,x_opt);
if Plot then subplot(2,2,2); plot_pipe_graph(g_opt,'test for fsolve - with analytical Jacobian','X','Y'); end

printf('fsolver with Jacobian - result: info = %d, norm(res) = %f, elapsed time = %f\n',info, norm(res_opt), t);


// With finite differences Jacobian and with fsolve
tic();
[x_opt, res_opt, info] = fsolve(x0,fsolve_obj_pipe,fsolve_dobj_diff_pipe,1e-6);
t = toc();

g_opt = pipe_update_graph(g_in,x_opt);
if Plot then subplot(2,2,3); plot_pipe_graph(g_opt,'test for fsolve - with finite diff Jacobian','X','Y'); end

printf('with Jacobian - result: info = %d, norm(res) = %f, elapsed time = %f\n',info, norm(res_opt), t);

if UseOptim then
  // With optim
  tic();
  [res_opt, x_opt] = optim(optim_pipe,'b',lower,upper,x0,'gc');
  t = toc();

  g_opt = pipe_update_graph(g_in,x_opt);
  if Plot then subplot(2,2,4); plot_pipe_graph(g_opt,'test for fsolve - with optim','X','Y'); end

  printf('with optim - result: norm(res) = %f, elapsed time = %f\n',res_opt, t);
end

