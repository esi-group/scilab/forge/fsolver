mode(-1);
//////////////////////////////////////
// Generalized Rosenbrock function 
/////////////////////////////////////
printf('\n\nRunning Generalized Rosenbrock demo\n');
demodir = get_absolute_file_path('fsolver_large.dem.sce');

exec(demodir+'generalrosenbrock.sci');

// before we solve the large rosenbrock problem increase the stacksize 
sz = stacksize(); 
newsize = 134217728;
stacksize(newsize);

//for k=1:10
// uncomment for good test 
n = 2^20;

x0(1:n,1) = -1.9;
x0(2:2:n,1) = 2; 

options = init_param();
options = add_param(options,'TolFun',1e-6);
options = add_param(options,'Jacobian','on');


printf('\n\nSolving system of %d equations with sparse derivatives\n',n);

tic();
[x,fval,exitflag,status] = fsolver(genrosenbrock,x0,options);
timetaken = toc();

printf('norm(x-1)  = %e\n', norm(x-1));
printf('norm(f)  = %e\n', norm(fval));
printf('exitflag = %d\n', exitflag);
printf('nfev     = %d\n', status('nfev'));
printf('time     = %g\n', timetaken);
//end


