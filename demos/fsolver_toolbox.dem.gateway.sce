// ====================================================================
// Yann COLLETTE
// Christopher MAES
// Copyright 2009
// This file is released into the public domain
// ====================================================================
demopath = get_absolute_file_path("fsolver_toolbox.dem.gateway.sce");

subdemolist = ["demo fsolver",       "fsolver.dem.sce"];

subdemolist(:,2) = demopath + subdemolist(:,2);
// ====================================================================
