#include <string.h>
#include <setjmp.h>
#include <stdio.h>


/* include files for scilab c interface */ 
#include <stack-c.h> 
#include <Scierror.h>
#define  _(A) (A)    /* This will be replaced for localization */ 
#include <MALLOC.h> 


#include "helper.h"   /* for the options parameters */ 

#define DEBUG 1      /* turn on debugging sciprints */ 

/* The following defines the order of input/output variables on the stack */ 
#define J_IN 1 
#define COLORING_OUT 2
#define NUMCOLORS_OUT 3 
#define MINCOLORS_OUT 4 

/* sci_columncolor will be called from scilab as follows:
   [coloring,numcolors,mincolor] = columncolor(J)

   Input:   J           A sparse m x n Jacobian matrix 

   Output:  coloring    A coloring of the columns of J. Of length n. 
            numcolors   The number of colors used in the coloring 
            mincolor    A lower bound on the minimum number of colors
                        required to color the columns of Jacobian

   columncolor is a wrapper around the fortran subroutine DSM. 
*/
int sci_columncolor(char *fname){
  /* The following variables are use to get and set scilab variables ...
    on the stack */ 
  int m_J, n_J;
  int m_coloring, n_coloring, l_coloring;
  int m_numcolors, n_numcolors, l_numcolors; 
  int m_mincolors, n_mincolors, l_mincolors; 

  SciSparse J;

  /* The following variables are used for the call to DSM */
  int m, n, nnz; 
  int *indrow, *indcol;
  int *ngrp;
  int maxgrp; 
  int mingrp; 
  int info; 
  int *ipntr;
  int *jpntr;
  int *iworkarray; 
  int lenwrk;

  /*These variables are used for converting from Scilab's sparse matrix format*/ 
  int row_start, l, i, k;
  
  /* ensure the correct number of input and output arguments */ 
  CheckRhs(1,1);
  CheckLhs(1,3);

  /* check the type of J */ 
  if (GetType(J_IN) != sci_sparse){
    Scierror(999,_("%s: Wrong type of input argument #%d: A sparse matrix expected.\n"),fname,J_IN);
    return -1;
  }
  
  /* get J from the stack */ 
  GetRhsVar(J_IN, SPARSE_MATRIX_DATATYPE, &m_J, &n_J, (int *)&J);

  /* Scilab stores its matrices in a nonstandard Compressed Sparse Row format */ 
  /* mnel[i]  number of elements in row i, of size m                          */ 
  /* icol[l]  the column of the lth nonzero element of size nnz               */ 
  /* R[l]     the real value of the lth nonzero element of size nnz           */ 

  /* Get m and n */ 
  m = J.m;
  n = J.n; 

  /* Get the number of nonzeros in J */
  nnz = J.nel; 

  /* Make indcol point to icol */ 
  indcol = J.icol;

  /* Allocate memory for indrow*/ 
  indrow = (int *) MALLOC(sizeof(int)*nnz);
  
  /* To fill in indrow we need to do a little work */ 
  row_start = 0; 
  l = 0; 
  for(i=0; i<J.m; i++){
    for(k=row_start;k<row_start+J.mnel[i];k++){
      indrow[l] = i+1;
      l++;
    }
    row_start = row_start+J.mnel[i];
  }

  /* Create coloring variable on the stack */ 
  m_coloring = n;
  n_coloring = 1;
  CreateVar(COLORING_OUT,MATRIX_OF_INTEGER_DATATYPE,&m_coloring,&n_coloring,&l_coloring);
  
  /* Make ngrp point to the variable on the stack */ 
  ngrp = istk(l_coloring);

  /* Create numcolors variable on the stack */ 
  m_numcolors=1;
  n_numcolors=1;
  CreateVar(NUMCOLORS_OUT,MATRIX_OF_INTEGER_DATATYPE,&m_numcolors,&n_numcolors,&l_numcolors);

  /* Create mincolors variable on the stack */ 
  m_mincolors=1;
  n_mincolors=1;
  CreateVar(MINCOLORS_OUT,MATRIX_OF_INTEGER_DATATYPE,&m_mincolors,&n_mincolors,&l_mincolors);

  /* Allocate space for J and J^T in compressed form */ 
  ipntr = (int *) MALLOC(sizeof(int)*(m+1));
  jpntr = (int *) MALLOC(sizeof(int)*(n+1));

  /* Calculate the size of the workspace */ 
  lenwrk = m; 
  if (lenwrk < 6*n) 
    lenwrk = 6*n;

  /* Allocate memory for the workspace */ 
  iworkarray = (int *) MALLOC(sizeof(int)*lenwrk);

#if DEBUG 
  sciprint("Making call to DSM nnz:%d m:%d n:%d\n",nnz,m,n);
#endif 

  /* Make the call to DSM */ 
  C2F(dsm)(&m,&n,&nnz,indrow,indcol,ngrp,&maxgrp,&mingrp,&info,ipntr,jpntr,iworkarray,&lenwrk);

#if DEBUG  
  sciprint("DSM returned %d\n",info);
#endif 

  /* Setup outputs */ 
  if (Lhs >= 1){
    LhsVar(1) = COLORING_OUT;
  }
  if (Lhs >= 2){
    *istk(l_numcolors) = maxgrp; 
    LhsVar(2) = NUMCOLORS_OUT;
  }
  if (Lhs >= 3){
    *istk(l_mincolors) = mingrp;
    LhsVar(3) = MINCOLORS_OUT;
  }

  /* Clean up */ 
  FREE(indrow);
  FREE(ipntr);
  FREE(jpntr);
  FREE(iworkarray);

  return 0;
}
