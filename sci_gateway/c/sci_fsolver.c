#include <string.h>
#include <setjmp.h>
#include <stdio.h>

/* include files for scilab c interface */ 
#include <stack-c.h> 
#include <Scierror.h>
#define  _(A) (A)    /* This will be replaced for localization */ 
#include <MALLOC.h> 


#include "helper.h"   /* for the options parameters */ 

#include "spartan.h"  /* for the spartan sparse solver */ 

#define DEBUG 1      /* turn on debugging sciprints */ 

/* The following prototypes define functions that wrap calls to the user's function */
static int userfunwrappernd(int *n, const double *x, double *f, int *flag); 
static int userfunwrapperdd(int *nptr, double *x, double *f, double *jac, int *ldfjac,
                     int *iflag);
static int getuserjactype(int n, double *x, int *nnz);
static int spartan_userfunwrapper(double *f, const double *x, spartan_index n);
static int spartan_userjacobianwrapper(spartan_sparse **J, const double *x, spartan_index n);


/* The following prototypes define routines that setup and call the solvers */
static int densesolver    (int n, double *x0, double *f, double tol, int *info);
static int densefdsolver  (int n, double *x0, double *f, double tol, int *info);
static int sparsesolver   (int n, double *x0, double *f, double tol, int *info); 
static int sparsefdsolver (int n, double *x0, double *f, double tol, int *info, int jacpattern);

/* The following defines the order of input/output variables on the stack */ 
#define F_IN         1
#define X0_IN        2
#define PARAM_IN     3
#define X_OUT        4
#define FVAL_OUT     5
#define EXIT_OUT     6
#define NFEV_OUT     7
#define STATUS_OUT   8
#define X_SCILAB     9
#define J_SCILAB     10 

/* The following constants signal a sparse or dense Jacobian matrix */ 
#define JACDENSE     1
#define JACSPARSE    2 

static char *global_fname;    /* A pointer to string containing the name of the function; 
                                 used for outputing Scilab errors inside of subroutines */
static int userfunpointer;    /* A pointer to the userfun is stored in this static var*/
static int nfeval;            /* The number of userfun evaluation is stored in this static var*/
static int nnzjac;            /* The number of nonzeros in the user-supplied jacobian */
static int stackbytes;        /* The number of bytes used on the scilab stack during call */
static int heapbytes;         /* The number of bytes used on the heap during the call */  
static int *coloring;         /* The column coloring of the Jacobian pattern */ 
static int numcolors;         /* The number of colors in the coloring */ 
static int printiter;         /* Indicates whether the solver should print information */
static int maxfunevals;       /* The maxium number of function evaluations */

#define DEFAULT_TOLERANCE 1e-6 /* The tolerance of the function value */
#define MAX_FUN_MULT      100  /* The maximum number of function evalutations is MAX_FUN_MULT*n */

/* sci_fsolver will be called from scilab as follows: 
   [x, fval, exitflag, status] = fsolver_gateway(userfun, x0, options)

   Input variables:   userfun     a scilab function in the form

                                  f = userfun(x) 
                                  
                                  where userfun evaluates the nonlinear function 
                                  at the point x at returns the result in f 
                                  
                                  Or,  [f,J] = userfun(x) 

                                  where userfun evalutes the nonlinear function 
                                  and the Jacboian at the point x.  In the 
                                  second case userfun should be in the form 
                                  
                                  function [f, J] = usefun(x)
                                          // evaluate f(x) store the result in f 
                                          nargout = argn(1);
                                          if (nargout > 1 ) 
                                             //evaluate J(x) store the result in J 
                                          end
                                  endfunction 

                     x0          An initial guess of the solution. x0 is of size n.

                     options     A parameter list of options for the solver.
  
  Output variables:
                    x           The computed solution of the system, i.e. the x 
                                such that f(x) =~ 0. x is of size n. 
 
                    fval        The value of the function at the computed 
                                solution, fval = f(x). fval is of size n. 

                    exitflag    A flag that indicates success or failure of 
                                the solver 
                       
                    status      A parameter list that contains additional 
                                information about the solve.
*/                     
int sci_fsolver(char *fname){

  /* the following variables are used to create scilab variables on the stack */ 
  int m_x0, n_x0, l_x0;
  int m_f, n_f, l_f;
  int m_x, n_x, l_x; 

  int m_userfun = 0, n_userfun = 0, l_userfun = 0;

  int m_exitflag, n_exitflag, l_exitflag;
  int m_nfev = 1, n_nfev = 1, l_nfev,  l_list_nfev;

  /* the following variables are related to options */ 
  int option_found;
  char *JacobianOption = NULL;
  char *DisplayOption  = NULL;

  /* these are the mathematical variables */ 
  int n;   /* the size of the system */ 
  double tolerance;  /* the tolerance on norm(f) */
  int maxfuns; 
  int exitflag;
  
  /* these variables are for the status parameter list */
  int m_extra       = 2, n_extra       = 1, l_extra; 
  int m_list_labels = 2, n_list_labels = 1;

  static const char * ListLabels [] = {"plist","nfev"};

  global_fname = fname;   /* Set global_fname to fname for reporting errors */
 
  /* We first ensure the correct number of input and output arguments */ 
  CheckRhs(3,3); 
  CheckLhs(1,4);

  /* Then we clear memory counters */ 
  stackbytes = 0;
  heapbytes  = 0;

  /* Then we get the input arguments off the stack */ 
  /* Starting with the scilab function*/ 
  if ( GetType(F_IN) != sci_c_function){
    Scierror(999,_("%s: Wrong type for input argument #%d: A scilab function expected.\n"), fname, 1);
    return 0;
  }
  GetRhsVar(F_IN, EXTERNAL_DATATYPE, &m_userfun, &n_userfun, &l_userfun);
  userfunpointer = l_userfun; 
  nfeval = 0;

  /* Then the initial guess x0 */ 
  if ( GetType(X0_IN) != sci_matrix ){
    Scierror(999,_("%s: Wrong type for input argument #%d: A vector expected.\n"), fname, 2);
    return 0;
  }
  GetRhsVar(X0_IN, MATRIX_OF_DOUBLE_DATATYPE, &m_x0, &n_x0, &l_x0);
  if ( n_x0 != 1 ){
    Scierror(999,_("%s: Wrong size for input argument #%d: A vector of size n x 1 expected.\n"),fname,2);
    return 0;
  }
  /* We store the size of x0 as the size of the system of nonlinear equations */ 
  n = m_x0; 
  maxfuns = MAX_FUN_MULT*n;

  /* Then the options parameter */  
  /* Here we use macros defined in helper.h */ 
  INIT_PARAM(PARAM_IN);
  if (strncmp(LabelList[0],"plist",5) != 0){
    Scierror(999,_("%s: Wrong type for input argument #%d: A parameter list exepected.\n"),fname,3);
    return 0;
  }

  /* Now we need to process the options */ 
  GET_PARAM_STRING("Jacobian",JacobianOption,"off", option_found);
  GET_PARAM_STRING("Display" ,DisplayOption, "off", option_found);
  GET_PARAM_DOUBLE("TolFun"  ,tolerance,    DEFAULT_TOLERANCE, option_found);
  GET_PARAM_INT("MaxFunEvals",maxfunevals,maxfuns, option_found);

  /* We create the output variables on the stack before the call to the solver */ 
  /* The x solution variable */ 
  m_x = n; 
  n_x = 1; 
  CreateVar(X_OUT, MATRIX_OF_DOUBLE_DATATYPE, &m_x, &n_x, &l_x);
  /* We copy x0 into x in preperation of the solver call */ 
  memcpy(stk(l_x),stk(l_x0),sizeof(double)*n);
  stackbytes += sizeof(double)*n;

  /* The fval variable */ 
  m_f = n; 
  n_f = 1; 
  CreateVar(FVAL_OUT, MATRIX_OF_DOUBLE_DATATYPE, &m_f, &n_f, &l_f);
  stackbytes += sizeof(double)*n;

  /* The exitflag variable */
  m_exitflag = 1; 
  n_exitflag = 1;
  CreateVar(EXIT_OUT, MATRIX_OF_INTEGER_DATATYPE, &m_exitflag, &n_exitflag, &l_exitflag);
  stackbytes += sizeof(int);

  /* A variable to hold the number of function evaluations */
  m_nfev = 1; 
  n_nfev = 1; 
  CreateVar(NFEV_OUT, MATRIX_OF_INTEGER_DATATYPE,&m_nfev,&n_nfev,&l_nfev);
  stackbytes += sizeof(int);

  /* The status parameter list */ 
  /* I would like to move this after the solver call but it needs to be here to 
     get everything on the stack in the right order. There must be a better way 
     to do this !!! */ 
  CreateVar(STATUS_OUT, MATRIX_ORIENTED_TYPED_LIST_DATATYPE, &m_extra, &n_extra, &l_extra);
  CreateListVarFromPtr(STATUS_OUT, 1, MATRIX_OF_STRING_DATATYPE,  &m_list_labels, &n_list_labels, ListLabels);
  CreateListVarFrom(STATUS_OUT,    2, MATRIX_OF_INTEGER_DATATYPE, &m_nfev, &n_nfev, &l_list_nfev, &l_nfev);
  stackbytes += sizeof(int);  /* Not sure exactly how much memory this requires */ 

  /* Recreate the nfev variable because once you call CreateListVarFrom, the nfev variable is erased.
     You cannot use anymore *istk(l_nfev). The nfev variable will be attached to the list at the end 
     of the gateway. */
  m_nfev = 1; 
  n_nfev = 1; 
  CreateVar(NFEV_OUT, MATRIX_OF_INTEGER_DATATYPE,&m_nfev,&n_nfev,&l_nfev);

  printiter = 0;
  if (strncmp(DisplayOption,"iter", 4) == 0)
    printiter = 1;
    
  /* We decide which solver to call based on the value of the Jacobian option */ 
  if (strncmp(JacobianOption, "off", 3) == 0){
    int jacpattern;

    /* Check to see if the user has set the JacobPattern option */
    jacpattern = find_label(LabelList,m_param,"JacobPattern");
    if (jacpattern == -1) {
      /* The user did not specify a sparsity pattern. 
         Call a dense finite-difference solver */
      if(densefdsolver(n,stk(l_x),stk(l_f),tolerance,istk(l_exitflag))) return 0;
    }
    else {
      /* The user specifes the derivatives are sparse and provides the 
         sparsity pattern. Call a sparse finite-difference solver */ 
      if(sparsefdsolver(n,stk(l_x),stk(l_f),tolerance,istk(l_exitflag),jacpattern)) return 0;
    }
  }
  else if (strncmp(JacobianOption,"on", 2) == 0){
    /* We evaluate the users Jacobian to see whether it is sparse or dense */ 
    int nnz;
    int jactype; 

    /* First check to make sure the userfun returns two output args */ 
    if (m_userfun != 2){
      Scierror(999,_("%s: Wrong type for input argument #%d: A scilab function with two output arguments expected.\n"), fname, 2);
      return 0; 
    }

    jactype = getuserjactype(n,stk(l_x),&nnz);
    if (jactype == JACDENSE){
      /* The user provided a dense Jacobian. Call a dense solver. */ 
      if(densesolver(n,stk(l_x),stk(l_f),tolerance,istk(l_exitflag))) return 0;
    }
    else {  
      /* The user provided a sparse Jacobian. Call a sparse solver. */
      if(sparsesolver(n,stk(l_x),stk(l_f),tolerance,istk(l_exitflag))) return 0;
    }
  }
  else{
    Scierror(999,_("%s: Bad Jacobian option.\n"),fname);
    return 0;
  }
  
  if (Lhs >= 1)
    LhsVar(1) = X_OUT;
  if (Lhs >= 2)
    LhsVar(2) = FVAL_OUT;
  if (Lhs >= 3)
    LhsVar(3) = EXIT_OUT;
  if (Lhs >= 4){
    LhsVar(4) = STATUS_OUT;
    /* Store the number of function evaluations in status parameter list */
    CreateListVarFromPtr(STATUS_OUT, 1, MATRIX_OF_STRING_DATATYPE,  &m_list_labels, &n_list_labels, ListLabels);
    *istk(l_nfev) = nfeval;
    CreateListVarFrom(STATUS_OUT,    2, MATRIX_OF_INTEGER_DATATYPE, &m_nfev, &n_nfev, &l_list_nfev, &l_nfev);
  }

  return 0;

}

int densesolver    (int n, double *x, double *f, double tol, int *info){
  /*int densesolver    (int n, int l_x, int l_f, int l_exitflag, int l_nfev, char *fname){*/
  /* The user provided a dense Jacobian so we call a solver based on dense derivatives */ 
      
  /* The following variables are needed for the solver call */ 
  double *workarray = NULL; 
  int lenwa; 
  int ldfjac = n; 
  double *fjac = NULL; 

  /* Allocate workspace memory for solver */ 
  lenwa     =  ceil((n*(n+13))/2); /* required amount of workspace for hybrj */ 
  workarray =  MALLOC(sizeof(double)*lenwa);  
  heapbytes += sizeof(double)*lenwa;
  /* Check for out of memory error */
  if (workarray == NULL){
    Scierror(999,_("%s: Unable to allocate %d bytes on heap. At least %d bytes required.\n"), 
             global_fname, sizeof(double)*lenwa, heapbytes);
    return -1;
  }

  /* allocate space for the dense jacobian */ 
  fjac = MALLOC(sizeof(double)*n*n);
  heapbytes += sizeof(double)*n*n; 
  /* Check for out of memory error */ 
  if (fjac == NULL){
    Scierror(999,_("%s: Unable to allocate %d bytes on heap. At least %d bytes required.\n"), 
             global_fname, sizeof(double)*n*n, heapbytes);
    /* we could have malloced workarray so try to clean this up */ 
    FREE(workarray);
    return -1;
  }


  /* A call to the solver will require this much memory on the stack */
  stackbytes += sizeof(double)*(n + 1 + n*n); /* memory used in userfunwrapperdd */ 
#if DEBUG 
  sciprint("%d bytes required on scilab stack\n",stackbytes);
  sciprint("%d bytes required on the heap\n", heapbytes);
#endif 


#if DEBUG
  sciprint("Calling hybrj1\n");
#endif 
  C2F(hybrj1)(userfunwrapperdd,&n,x,f,fjac,&ldfjac,&tol,info,workarray,&lenwa);
#if DEBUG
  sciprint("Hybrj1 returned %d\n",*info);
#endif
  FREE(workarray);
  FREE(fjac);
  return 0;
}

int densefdsolver  (int n, double *x, double *f, double tol, int *info){
  /* The following variables are needed for the solver call */ 
  double *workarray = NULL;
  int lenwa; 

  lenwa     =  ceil((n*(3*n+13))/2); /* required amount of workspace for hybrd */ 
  workarray =  MALLOC(sizeof(double)*lenwa);  /* Allocate workspace memory for solver */ 
  heapbytes += sizeof(double)*lenwa;
  /* Check for out of memory error */ 
  if (workarray == NULL){
    Scierror(999,_("%s: Unable to allocate %d bytes on heap. At least %d bytes required.\n"), 
             global_fname, sizeof(double)*lenwa, heapbytes);
    return -1;
  }

  /* A call to the solver will require this much memory on the stack */
  stackbytes += sizeof(double)*(n); /* Memory used in userfunwrappernd */ 
#if DEBUG 
  sciprint("%d bytes required on scilab stack\n",stackbytes);
  sciprint("%d bytes required on the heap\n", heapbytes);
#endif 

#if DEBUG
  sciprint("Calling Hybrd1\n");
#endif 
  C2F(hybrd1)(userfunwrappernd,&n,x,f,&tol,info,workarray,&lenwa);
#if DEBUG
  sciprint("Hybrd1 returned %d\n",*info);
#endif 
  FREE(workarray);
  return 0;
}

/* pass this function to spartan if the 'Display' option is 
   equal to 'iter' */
void scilab_printstring(const char *string){
  sciprint("%s\n",string);
}
/* pass this function to spartan if the 'Display' option 
   not equal to 'iter' */
void scilab_printnull(const char *string){
  return;
}

int sparsesolver   (int n, double *x, double *f, double tol, int *info){ 
  /* The following variables are needed for the solver call*/
  int ierr;
  int j;
  double opts[SPARTAN_OPTIONS];

  /* zero opts for default options */ 
  for(j=0;j<SPARTAN_OPTIONS;j++) opts[j] = 0.;

  /* Tell spartan to use the tolerance provided by the user */
  opts[SPARTAN_TOL] = tol;

  /* Tell spartan to free the Jacobian after use */
  opts[SPARTAN_MEMMGMT] = 1;

  /* Setup printing */ 
  if (printiter)
    spartan_setprintfunction(scilab_printstring);
  else
    spartan_setprintfunction(scilab_printnull);

  /* Print out memory usage */ 
#if DEBUG 
  sciprint("%d bytes required on scilab stack\n",stackbytes);
  sciprint("%d bytes required on the heap\n", heapbytes);
#endif 

#if DEBUG 
  sciprint("Calling spartan solver\n");
#endif 
  /* Make the call to the solver */
  ierr = spartan_solve(spartan_userfunwrapper, spartan_userjacobianwrapper, n, f, x, opts);
#if DEBUG 
  sciprint("spartan finished with ierr=%d\n",ierr);
#endif 

  /* Set solver exit information */ 
  *info = ierr;
    
  /* Clean up */
  return 0;
}

int sparsefdsolver (int n, double *x, double *f, double tol, int *info, int paramloc){
  int m_j, n_j; 
  SciSparse JacobPattern;
  spartan_sparse *Jpat_t, *Jpat_c;
  int m, i, j,row_start,l,k,nnz;
  int mincolors;
  int ierr;

  /* Solver variables */
  double opts[SPARTAN_OPTIONS];
  spartan_jacobian sparsefdjacobian;

  /* Grab the Jacobian Pattern */ 
  GetListRhsVar(PARAM_IN,paramloc+1, SPARSE_MATRIX_DATATYPE, &m_j, &n_j, (int *)&JacobPattern);

  /* Check that the Jacobian Pattern is a square matrix */ 
  if (m_j != n_j){
    Scierror(999,_("%s: The sparsity pattern must be a square sparse matrix.\n"),global_fname);
    return -1;
  }

  /* Get dimensions */
  m = JacobPattern.m;

  /* Get number of nonzeros */
  nnz = JacobPattern.nel;
  
  Jpat_t = spartan_spalloc(n,n,nnz,1,1);
  heapbytes += sizeof(int)*(n+1 + 2*nnz);
  /* Check for out of memory error */ 
  if (!Jpat_t){
    Scierror(999,_("%s: Unable to allocate %d bytes on heap. At least %d bytes required.\n"), 
             global_fname, sizeof(int)*(n+1 + 2*nnz), heapbytes);
    return -1;
  }

  /* Scilab stores its matrices in a nonstandard Compressed Sparse Row format */ 
  /* mnel[i]  number of elements in row i, of size m                          */ 
  /* icol[l]  the column of the lth nonzero element of size nnz               */ 
  /* R[l]     the real value of the lth nonzero element of size nnz           */ 
  /* Note that Scilab uses Fortran style (1-based) indexing for icol          */

  /* Convert JacobPattern to triplet form */ 
  row_start = 0;
  l = 0;
  for(i=0; i<n; i++){
    for(k=row_start; k < row_start + JacobPattern.mnel[i]; k++){
      spartan_entry(Jpat_t,i,JacobPattern.icol[l] - 1,JacobPattern.R[k]);
      l++; 
    }
    row_start = row_start + JacobPattern.mnel[i];
  }

  /* Allocate space for the coloring in the global variable coloring*/ 
  coloring = MALLOC(sizeof(int)*n);
  heapbytes += sizeof(int)*n; 
  /* Check for out of memory error */ 
  if (coloring == NULL){
    Scierror(999,_("%s: Unable to allocate %d bytes on heap. At least %d bytes required.\n"), 
             global_fname, sizeof(int)*n, heapbytes);
    spartan_spfree(Jpat_t);
    return -1;
  }

#if DEBUG 
  sciprint("Making call to DSM nnz:%d m:%d n:%d\n",nnz,m,n);
#endif 
  
    /* Compute a column coloring of the Jacobian pattern */ 
  ierr = spartan_columncolor(coloring,&numcolors,&mincolors,Jpat_t);

#if DEBUG  
  sciprint("DSM returned %d\n",ierr);
#endif 

#if DEBUG 
  sciprint("Using %d colors; lower bound %d\n",numcolors,mincolors);
#endif

  /* Convert Jpat_t from triplet form to compressed col form */ 
  Jpat_c = spartan_compress(Jpat_t);
  /* Free Jpat_t */ 
  Jpat_t = spartan_spfree(Jpat_t);
  /* Get the function to compute the sparse Jacobian via finite differences */
  sparsefdjacobian = spartan_initsfd(spartan_userfunwrapper, Jpat_c, coloring, numcolors);  

  /* zero opts for default options */ 
  for(j=0;j<SPARTAN_OPTIONS;j++) opts[j] = 0.;
  
  /* Tell spartan to use the tolerance provided by the user */
  opts[SPARTAN_TOL] = tol;

  /* Setup printing */ 
  if (printiter)
    spartan_setprintfunction(scilab_printstring);
  else
    spartan_setprintfunction(scilab_printnull);


  /* Print out memory usage */ 
#if DEBUG 
  sciprint("%d bytes required on scilab stack\n",stackbytes);
  sciprint("%d bytes required on the heap\n", heapbytes);
#endif 
  
#if DEBUG 
  sciprint("Calling spartan solver\n");
#endif 
  ierr = spartan_solve(spartan_userfunwrapper, sparsefdjacobian, n, f, x, opts);
#if DEBUG 
  sciprint("spartan finished with ierr=%d\n",ierr);
#endif 

  /* Set solver exit information */ 
  *info = ierr;

  /* Clean up */ 
  spartan_spfree(Jpat_c);

  /* Coloring variables */
  FREE(coloring);
  return 0;
}


/* userfunwrappernd is a wrapper for a scilab user function in the form 
   function  f=userfun(x) 
   
   userfunwrappernd is called directly by the solver. It in turn calls the 
   scilab function (userfun).  Here the suffix nd indicates that the user 
   has provided no derivatives. Note that since the solver is written in 
   Fortran, the arguments are passed in by reference. Also note that 
   userfunwrappernd uses the file global variable userfunpointer, this 
   must be set by fsolver_simple before a call to userfunwrappernd.
*/

int userfunwrappernd (int *nptr, const double *x, double *f, int *flag){
  
  int positionFirstElementOnStackForScilabFunction = X_SCILAB;
  int numberOfRhsOnScilabFunction = 1;
  int numberOfLhsOnScilabFunction = 1;
  int pointerOnScilabFunction = userfunpointer;
  int m_f, n_f, l_f; 
  int m_x, n_x, l_x; 
  int n; 

  n = *nptr;   /* define n just for ease of notation */ 
  /* Ensure we won't exceed the maximum number of function evals */
  if (nfeval + 1 > maxfunevals){
    *flag = -1;
    return(-1);
  }


  /* create a variable on the stack to store x */ 
  m_x = n;
  n_x = 1; 
  CreateVar(X_SCILAB,"d",&m_x,&n_x,&l_x); 
  /* copy the input argument x into the scilab variable */
  memcpy(stk(l_x),x,sizeof(double)*n);

  /* evaluate f = userfun(x)  */
  /* C2F(scifunction) call a scilab function */
  C2F(scifunction)(&positionFirstElementOnStackForScilabFunction,
                   &pointerOnScilabFunction,
                   &numberOfLhsOnScilabFunction,
                   &numberOfRhsOnScilabFunction);	
  nfeval++;
  
  /* result f is now in X_SCILAB position on the stack */ 
  GetRhsVar(X_SCILAB, MATRIX_OF_DOUBLE_DATATYPE, &m_f, &n_f, &l_f);

  /* Copy result from stack into the f output array */
  memcpy(f, stk(l_f), sizeof(double)*n);

  return 0;
}

/* userfunwrapperdd is a wrapper for a scilab user function in the form 
   function [f,J] = userfun(x) 

   userfunwrapperdd is called directly by the solver. It in turn calls
   the scilab function (userfun). Here the suffix dd indicates the
   user has provided dense derivatives.

   The solver uses the input argument iflag to indicate when it needs
   to compute the jacobian as well as the function value.

   The following is from the minpack documentation:

   if iflag = 1 calculate the functions at x and return this vector in
   fvec. do not alter fjac.  

   if iflag = 2 calculate the jacobian at x and return this matrix in
   fjac. do not alter fvec.

   userfunwraperd uses the value of iflag to call userfun with one
   output argument if only function values are required and two output
   arguments if the jacobian is required.
*/

int userfunwrapperdd(int *nptr, double *x, double *f, double *jac, int *ldfjac, int *iflag){
  int positionFirstElementOnStackForScilabFunction = X_SCILAB;
  int numberOfRhsOnScilabFunction = 1;
  int numberOfLhsOnScilabFunction;
  int pointerOnScilabFunction = userfunpointer;
  int m_f, n_f, l_f; 
  int m_x, n_x, l_x; 
  int m_j, n_j, l_j; 
  int n; 

  n = *nptr;   /* define n just for ease of notation */ 

  /* Ensure we won't exceed the maximum number of function evals */
  if (nfeval + 1 > maxfunevals){
    *iflag = -1;
    return(-1);
  }
  

  /* create a variable on the stack to store x */ 
  m_x = n;
  n_x = 1; 
  CreateVar(X_SCILAB,"d",&m_x,&n_x,&l_x); 
  /* copy the input argument x into the scilab variable */
  memcpy(stk(l_x),x,sizeof(double)*n);


  /* decide the number of lhs variables based on iflag */ 
  if (*iflag == 1) 
    numberOfLhsOnScilabFunction = 1; 

  if (*iflag == 2) {
    numberOfLhsOnScilabFunction = 2; 

    /* we need to create a variable on the stack for the 
       jacobian output of the scilab function */
    m_j = n; 
    n_j = n; 
    CreateVar(J_SCILAB, "d", &m_j, &n_j, &l_j);
  }

  
  /* evaluate [f,J] = userfun(x)  */
  /* C2F(scifunction) call a scilab function */
  C2F(scifunction)(&positionFirstElementOnStackForScilabFunction,
                   &pointerOnScilabFunction,
                   &numberOfLhsOnScilabFunction,
                   &numberOfRhsOnScilabFunction);	
  nfeval++;

  if (GetType(X_SCILAB) != sci_matrix){
    Scierror(999,_("A vector expected as the first value returned by userfun\n"));
    return -1;
  }
    
  /* result f is now in X_SCILAB position on the stack */ 
  GetRhsVar(X_SCILAB, MATRIX_OF_DOUBLE_DATATYPE, &m_f, &n_f, &l_f);

  /* If iflag == 1 copy result from stack into the f output array */
  if (*iflag == 1) 
    memcpy(f, stk(l_f), sizeof(double)*n); 

  /* If iflag == 2 don't touch f, but copy result from stack into jac */
  if (*iflag == 2){
    if (GetType(J_SCILAB) != sci_matrix){
      Scierror(999,_("A matrix expected as the second value returned by userfun\n"));
      return -1;
    }
    /* result J is now in J_SCILAB position on the stack */ 
    GetRhsVar(J_SCILAB, MATRIX_OF_DOUBLE_DATATYPE, &m_j, &n_j, &l_j);    

    memcpy(jac,stk(l_j),sizeof(double)*n*n);
  }

  return 0;

}



int getuserjactype(int n, double *x, int *nnz){
  int positionFirstElementOnStackForScilabFunction = X_SCILAB;
  int numberOfRhsOnScilabFunction = 1;
  int numberOfLhsOnScilabFunction;
  int pointerOnScilabFunction = userfunpointer;
  int m_f, n_f, l_f; 
  int m_x, n_x, l_x; 
  int m_j, n_j, l_j; 


  SciSparse Jacobian;
  int i,l,k,row_start;

  /* create a variable on the stack to store x */ 
  m_x = n;
  n_x = 1; 
  CreateVar(X_SCILAB,MATRIX_OF_DOUBLE_DATATYPE,&m_x,&n_x,&l_x); 
  /* copy the input argument x into the scilab variable */
  memcpy(stk(l_x),x,sizeof(double)*n);

  /* decide the number of lhs variables based on iflag */ 
  numberOfLhsOnScilabFunction = 2; 

  /* This is a HACK!!! For some reason we need to create 
     a variable on the stack to get back both [f,J] 
     from userfun. It doesn't matter what type of 
     variable we create so here we create a single scalar.*/ 
  m_j = 1;
  n_j = 1;
  CreateVar(J_SCILAB,MATRIX_OF_DOUBLE_DATATYPE,&m_j,&n_j,&l_j);
  
  /* [f,J] = userfun(x)  */
  /* C2F(scifunction) call a scilab function */
  C2F(scifunction)(&positionFirstElementOnStackForScilabFunction,
                   &pointerOnScilabFunction,
                   &numberOfLhsOnScilabFunction,
                   &numberOfRhsOnScilabFunction);	
  nfeval++;

  if (GetType(X_SCILAB) != sci_matrix){
    Scierror(999,_("A vector expected as the first value returned by userfun\n"));
    return -1;
  }

  if (GetType(J_SCILAB) == sci_matrix){
    /* The users function supplies a dense matrix */ 
    *nnz = n*n;
    return JACDENSE;
  }
  else if (GetType(J_SCILAB) == sci_sparse){
    /* The users function supplies a sparse matrix */ 

    /* result J is now in J_SCILAB position on the stack */ 
    GetRhsVar(J_SCILAB, SPARSE_MATRIX_DATATYPE, &m_j, &n_j, (int *)&Jacobian);    
   
    if (m_j != n_j || m_j != n){
      Scierror(999,_("The sparse matrix returned by userfun must be of size n x n.\n"));
      return -1;
    }
    *nnz  = Jacobian.nel; 
    return JACSPARSE;
  }  
  else {
    Scierror(999,_("A sparse or dense matrix expected as the second value returned by userfun\n"));
    return -1;
  }
}


/* spartan_userfunwrapper is a wrapper for a scilab user function in the form 
   function [f] = userfun(x) 

   spartan_userfunwrapper is called directly by the spartan solver. It in turn calls
   the scilab function (userfun). 
*/
int spartan_userfunwrapper(double *f, const double *x, spartan_index n){
  int flag, status;  
  status = userfunwrappernd (&n,x,f, &flag);
  return(status);
}

/* spartan_userjacobianwrapper is a wrapper for a scilab user function in the form 
   function [f,J] = userfun(x) 

   spartan_userjacobianwrapper called directly by the spartan solver. It in turn calls
   the scilab function (userfun). 
*/
int spartan_userjacobianwrapper(spartan_sparse **J, const double *x, spartan_index ns){
  int positionFirstElementOnStackForScilabFunction = X_SCILAB;
  int numberOfRhsOnScilabFunction = 1;
  int numberOfLhsOnScilabFunction = 2;
  int pointerOnScilabFunction = userfunpointer;
  int m_f, n_f, l_f; 
  int m_x, n_x, l_x; 
  int m_j, n_j, l_j; 
  int n, nnz; 

  SciSparse Jacobian;
  int *w, l, k, i, row_start, p, m;
  int *Jp, *Ji;
  double *Jx;
  

  n = (int) ns;   /* define n just for ease of notation */ 
  m = n;          /* define m for notation as well */ 

  /* Ensure we won't exceed the maximum number of function evals */
  if (nfeval + 1 > maxfunevals)
    return(-1);

  /* create a variable on the stack to store x */ 
  m_x = n;
  n_x = 1; 
  CreateVar(X_SCILAB,MATRIX_OF_DOUBLE_DATATYPE,&m_x,&n_x,&l_x); 
  /* copy the input argument x into the scilab variable */
  memcpy(stk(l_x),x,sizeof(double)*n);

  /* This is a HACK!!! For some reason we need to create 
     a variable on the stack to get back both [f,J] 
     from userfun. It doesn't matter what type of 
     variable we create so here we create a single scalar.*/ 
  m_j = 1;
  n_j = 1;
  CreateVar(J_SCILAB,MATRIX_OF_DOUBLE_DATATYPE,&m_j,&n_j,&l_j);  
  /* [f,J] = userfun(x)  */
  /* C2F(scifunction) call a scilab function */
  C2F(scifunction)(&positionFirstElementOnStackForScilabFunction,
                   &pointerOnScilabFunction,
                   &numberOfLhsOnScilabFunction,
                   &numberOfRhsOnScilabFunction);	
  nfeval++;

  if (GetType(X_SCILAB) != sci_matrix){
    Scierror(999,_("A vector expected as the first value returned by userfun\n"));
    return -1;
  }
    
  if (GetType(J_SCILAB) != sci_sparse){
    Scierror(999,_("A sparse matrix expected as the second value returned by userfun\n"));
    return -1;
  }

  /* result J is now in J_SCILAB position on the stack */ 
  GetRhsVar(J_SCILAB, SPARSE_MATRIX_DATATYPE, &m_j, &n_j, (int *)&Jacobian);   

  if (m_j != n_j || m_j != n){
    Scierror(999,_("The sparse matrix returned by userfun must be of size n x n.\n"));
    return -1;
  }

  /* Get the number of nonzeros of the Scilab Jacobian */ 
  nnz = Jacobian.nel;

  /* Allocate space for the spartan Jacobian in compressed col format*/ 
  *J = spartan_spalloc(n,n,nnz,1,0); 
  
  /* Check for out of memory error */ 
  if (*J == NULL)
    return(-1); 

  /* For ease of notation */ 
  Jp = (*J)->p; Ji = (*J)->i; Jx = (*J)->x;

  /* Now we need to convert from Scilab's compressed row format to spartan's
     compressed column format  */
  /* Scilab stores its matrices in a nonstandard Compressed Sparse Row format */ 
  /* mnel[i]  number of elements in row i, of size m                          */ 
  /* icol[l]  the column of the lth nonzero element of size nnz               */ 
  /* R[l]     the real value of the lth nonzero element of size nnz           */ 
  /* Note that Scilab uses Fortran style (1-based) indexing for icol          */


  /* First we alloc a workspace */ 
  w = spartan_calloc(n,sizeof(int));
  
  /* Then we compute the column counts (using C indexing)*/ 
  for (l = 0; l < nnz; l++) w[Jacobian.icol[l]-1]++;

  /* Then we compute the column pointers by performing a cumulative 
     sum of the column counts, this is stored in both w and Jp */ 
  spartan_cumsum(Jp,w,n);

  /* Now we loop over each element in the matrix */
  row_start = 0;
  l = 0;   /* l will track our position in the length-nnz array */     
  for (i = 0; i < m; i++){
    for (k = row_start; k < row_start + Jacobian.mnel[i]; k++){
      p = w[Jacobian.icol[l]-1]++;     /* p points the position in the col */
      Ji[p] = i;                       /* Place (i,j)th entry */
      Jx[p] = Jacobian.R[l];           /* Store the numeric value */
      l++;
    }
    row_start = row_start + Jacobian.mnel[i];
  }

  /* we are finished free the workspace w */ 
  w = spartan_free(w);
  
  /* signal success */
  return(0);

}
