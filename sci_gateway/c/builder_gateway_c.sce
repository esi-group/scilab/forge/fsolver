// ====================================================================
// Yann COLLETTE
// Christopher MAES
// Copyright 2009
// This file is released into the public domain
// ====================================================================

fsolver_path = get_absolute_file_path('builder_gateway_c.sce');

library_name = 'scifsolver';
table = ['fsolver_gateway',          'sci_fsolver'; ...
         'columncolor',              'sci_columncolor'; ];

files = ['sci_fsolver.c','sci_columncolor.c','helper.c'];

libs    = ['../../src/spartan/libspartan', ...
           '../../src/minpack/libminpack' ];

ldflags = [];
fflags = ' -g';
cflags  = '-g -I' + fsolver_path;

// Add include path for spartan headers
cflags = cflags + ' -I' + fsolver_path + '/../../src/spartan/' 

tbx_build_gateway(library_name, table, files, fsolver_path,libs,ldflags,cflags,fflags);

clear tbx_build_gateway;
