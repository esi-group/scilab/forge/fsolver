Fsolver toolbox 
-----------------------

The Fsolver toolbox provides improved functionality for the solving
systems of nonlinear equations. In particular, Fsolver can efficently
handle nonlinear systems with sparse Jacobians. The main function is
`fsolver` which is meant to be a replacement for Scilab's `fsolve`.
Note, that `fsolver` has different arguments and options the Scilab's
`fsolve` function. However, `fsolver` has the same interface as
Matlab's `fsolve` function (contained in the Matlab's optimization
toolbox).

Running fsolver 
----------------------
To build, load, and run a demo of `fsolver` execute the following
command in the `fsolver_toolbox` directory:
    --> // compile and build the fsolver toolbox 
    --> exec builder.sce 
    --> // load the toolbox and help files into Scilab
    --> exec loader.sce
    --> // test the fsolver toolbox 
    --> cd demos/
    --> exec fsolver.dem.sce 


Contents of the toolbox 
------------------------
The following is a list and brief description of all files in the 
toolbox:

builder.sce                                   Main scilab script to build toolbox 

demos/                                        Directory of fsolver demonstartions
demos/badbroyden.sci                          Broyden tridiagonal test problem with error in Jacobian
demos/broydentridiagonal.sci                  Brodyen tridiagonal test problem 
demos/build_test.sci                          Support file for pipe problem
demos/compute_pipe_constraints.sci            Support file for pipe problem
demos/derivativecheck.dem.sce                 Demo of checkderivative function
demos/fsolve_pipe.sce                         Pipe test problem
demos/fsolver.dem.sce                         Main demonstration of fsolver functionality
demos/fsolver_large.dem.sce                   Demo of large scale problem (useful for detecting memory leaks)
demos/fsolver_sfd.dem.sce                     Demo of sparse finite-differences
demos/fsolver_toolbox.dem.gateway.sce         Script to install the demos into Scilab
demos/function6.sci                           Function 6 sparse test problem
demos/generalrosenbrock.sci                   Generalized Rosenbrock test problem 
demos/plot_pipe_graph.sci                     Support file for pipe problem
demos/powellsingular.sci                      Powell's singular test problem 
demos/singularbroyden.sci                     Broyden tridiagonal test problem that is singular at solution
demos/trigexp.sci                             TrigExp test problem 

etc/                                          Directory containing configuration files 
etc/fsolver_toolbox.quit                      Script to remove the fsolve toolbox (currently empty)
etc/fsolver_toolbox.start                     Script to start the fsolve toolbox 

help/                                         Directory containing help documentation for fsolver
help/builder_help.sce                         Script to build the english and french help files 
help/en_US/build_help.sce                     Script to build the english help file
help/en_US/fsolver.xml                        Main help file for fsolver
help/fr_FR/build_help.sce                     Script to build the french help file (currently no french help file)

macros/                                       Directory contain Scilab (.sci) macros/functions 
macros/buildmacros.sce                        Script to build the macros file 
macros/checkderivative.sci                    Function to check sparse and dense derivatives against FD estimate
macros/cleanmacros.sce                        Script to remove compiled macro (.bin) files 
macros/fdjac.sci                              Function to estimate dense Jacobian via finite-differences (FD)
macros/fsolver.sci                            Main fsolver macro; checks inputs and calls fsolver_gateway
macros/sfdjac.sci                             Function to estimate sparse Jacobian via sparse FDs

sci_gateway/                                  Directory containing gateway interface between Scilab and solvers
sci_gateway/builder_gateway.sce               Script to build gateway interface
sci_gateway/c/builder_gateway_c.sce           Script to build the C gateway interface
sci_gateway/c/helper.c                        Source file for parameter (options) interface   
sci_gateway/c/helper.h                        Definitions of GET_PARAM_* macros used to access parameters
sci_gateway/c/sci_columncolor.c               Gateway to dsm subroutine (mapped to columncolor in Scilab)
sci_gateway/c/sci_fsolver.c                   Gateway to hybrd, hybj, and spartan solvers (fsolver_gateway in Scilab)

src/                                          Directory contain external library (solver) source code
src/builder_src.sce                           Script to build library source code 

src/minpack/                                  Directory contain minpack library code (hybrd,hybrj solvers)
src/minpack/builder_minpack.sce               Script to build libminpack.so 
src/minpack/chkder.f                          Routine to check user-supplied derivative (not currently used)
src/minpack/dogleg.f                          Routine to compute dogleg solution to min ||Jp+f|| st ||Dp||<=Delta
src/minpack/enorm.f                           Routine to compute ||x|| robust to overflow
src/minpack/fdjac1.f                          Routine to estimate Jacobian via finite-differences
src/minpack/hybrd.f                           Routine to solve F(x)=0 when no derivatives are supplied
src/minpack/hybrd1.f                          Simplified interface to hybrd 
src/minpack/hybrj.f                           Routine to solve F(x)=0 when derivaties are supplied
src/minpack/hybrj1.f                          Simplified interface to hybrj
src/minpack/qform.f                           Routine to form dense Q (of QR decomposition) from factored form 
src/minpack/qrfac.f                           Routine to compute QR factorization of a matrix
src/minpack/test_pb.f                         Routines for minpack test funtions (not currently used)

src/spartan/                                  Directory containing the spartan solver 
src/spartan/618.f                             DSM routine for computing column-coloring of Jacobian matrix
src/spartan/Makefile                          Makefile for non-Scilab spartan demos and library
src/spartan/builder_spartan.sce               Build script for libspartan.so
src/spartan/demo.c                            Demo of spartan C interface
src/spartan/doc/                              Directory containing documentation and user guide for spartan
src/spartan/doc/Makefile                      Makefile for spartan documentation 
src/spartan/doc/references.bib                BibTeX file for documentation references
src/spartan/doc/spartan.tex                   TeX file for spartan documentation 
src/spartan/fddemo.c                          Demo of C interface for spartan's sparse finite-differences
src/spartan/matlab/                           Directory containing spartan MATLAB prototype
src/spartan/matlab/demo/broydentridiagonal.m  Broyden tridiagonal test problem 
src/spartan/matlab/demo/function6.m           Function6 test problem 
src/spartan/matlab/demo/generalrosenbrock.m   Generalized Rosenbrock test problem
src/spartan/matlab/demo/minpackatol.m         Minpack atol example test problem (from MINPACK User guide)
src/spartan/matlab/demo/powellsingular.m      Powell's singular test problem 
src/spartan/matlab/demo/simple.m              A simple test problem 
src/spartan/matlab/demo/singularbroyden.m     Broyden tridiagonal test problem (singular at solution)
src/spartan/matlab/demo/trigexp.m             TrigExp test problem 
src/spartan/matlab/spartan.m                  Matlab prototype of spartan (note: heavily documented)
src/spartan/matlab/spartan_dogleg.m           Matlab prototype for spartan's dogleg routine
src/spartan/matlab/spartantest.m              Matlab driver for running prototype
src/spartan/spartan.h                         Protypes of spartan function (must be included)       
src/spartan/spartan_blas.c                    Level-1 BLAS opertations used by spartan
src/spartan/spartan_columncolor.c             Routine to compute column coloring of sparse Jacobian
src/spartan/spartan_compress.c                Routine to convert triplet to compressed-col format 
src/spartan/spartan_cumsum.c                  Routine to compute cumlative sum of a vector 
src/spartan/spartan_dogleg.c                  Solve min ||Jp+f|| st ||Dp||<= Delta via Dogleg method
src/spartan/spartan_entry.c                   Routine to add nonzero entry to sparse matrix in triplet form
src/spartan/spartan_gatxpy.c                  Routine to compute y <- A'*x + y when A is sparse 
src/spartan/spartan_gaxpy.c                   Routine to compute y <- A*x  + y when A is sparse 
src/spartan/spartan_linsolve.c                Routine to solve A*x = b (via UMFPACK)
src/spartan/spartan_malloc.c                  Routines for allocating memory 
src/spartan/spartan_mex.c                     Matlab mex interface to spartan_dogleg
src/spartan/spartan_norms.c                   Routine to compute column norms of A 
src/spartan/spartan_print.c                   Routine to print output and set printing function 
src/spartan/spartan_sfdjac.c                  Routines to compute sparse finite-difference 
src/spartan/spartan_solve.c                   Routine to solve F(x) = 0 when J(x) is sparse 
src/spartan/spartan_util.c                    Routines to allocate and free sparse matrices 

tests/unit_tests/fsolver.tst                  Unit test for old fsolve function (not currently used)
